#include "dealer.h"
#include <cmath>
#include <time.h>
using namespace std;

Dealer::Dealer()
{
    m_showing = false;
}

Dealer::~Dealer()
{
    this->reset();
}

unsigned int Dealer::getNbCards() const
{
    return m_hand.size();
}

void Dealer::reset()
{
    while (m_hand.empty() == false) {
        delete m_hand.back();
        m_hand.pop_back();
    }
    m_showing = false;
    return;
}

Card *Dealer::getCard(unsigned int num) const
{
    return m_hand[num]; // list begin by 0!
}

Card *Dealer::getLastCard() const
{
    return m_hand.back();
}

Card *Dealer::getFirstCard() const
{
    return m_hand.front();
}

void Dealer::hit(Shoe *shoe, bool count)
{
    m_hand.push_back(new Card);
    m_hand.back()->setIdCard(shoe->draw(count));
    return;
}

void Dealer::seventeen(Shoe *shoe)
{
    while (this->total() < 17)
        this->hit(shoe);
    return;
}

unsigned int Dealer::total() const
{
    unsigned int total = 0;
    bool oneAs = false;
    for (unsigned int i = 0; i < m_hand.size(); i++) {
        if (m_hand.at(i)->isAs()) { // As
            if (!oneAs) {
                oneAs = true;
                total += m_hand.at(i)->value();
            } else {
                total += m_hand.at(i)->lowValue();
            }
        } else
            total += m_hand.at(i)->value();
    }
    if (oneAs && total > 21)
        return this->softTotal();
    else
        return total;
}

unsigned int Dealer::softTotal() const
{
    unsigned int total = 0;
    for (unsigned int i = 0; i < m_hand.size(); i++) {
        if (m_hand.at(i)->lowValue() == 0)
            total += m_hand.at(i)->value();
        else
            total += m_hand.at(i)->lowValue();
    }
    return total;
}

bool Dealer::hasTwentyOne() const
{
    return (this->total() == 21);
}

bool Dealer::hasBlackJack() const
{
    return (this->total() == 21 && m_hand.size() == 2);
}

bool Dealer::hasBust() const
{
    return (this->total() > 21);
}

bool Dealer::isShowing() const
{
    return m_showing;
}

void Dealer::flip()
{
    m_showing = true;
}

std::string Dealer::totalS() const
{
    if (!m_showing)
        return "";
    if (total() == softTotal())
        return to_string(total());
    else
        return to_string(softTotal()) + " / " + to_string(total());
}
