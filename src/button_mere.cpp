#include "button_mere.h"

ButtonMere::ButtonMere()
{
}

ButtonMere::ButtonMere(int x, int y, int w, int h, const std::function<void()> &click, Uint8 r, Uint8 g, Uint8 b)
{
    m_pos.x = x;
    m_pos.y = y;
    m_pos.w = w;
    m_pos.h = h;
    onClick = click;
    m_r = r;
    m_g = g;
    m_b = b;
    m_active = true;
}

ButtonMere::~ButtonMere()
{
}

void ButtonMere::set(int x, int y, int w, int h, const std::function<void()> &click, Uint8 r, Uint8 g, Uint8 b)
{
    m_pos.x = x;
    m_pos.y = y;
    m_pos.w = w;
    m_pos.h = h;
    onClick = click;
    m_r = r;
    m_g = g;
    m_b = b;
    m_active = true;
}

void ButtonMere::show(GameEngine *ge) const
{
    if (!m_active)
        return;
    ge->fillRect(m_pos, m_r, m_g, m_b);
    ge->drawRect(m_pos, 0, 0, 0);
}
