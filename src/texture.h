#ifndef TEXTURE_H
#define TEXTURE_H

#include <SDL2/SDL.h>
#include <iostream>
#include <string>

class Texture
{
private:
    SDL_Renderer *renderer = nullptr;
    SDL_Texture *texture = nullptr;
    std::string name{};
    int width{0};
    int height{0};

public:
    Texture();
    ~Texture();

    bool LoadTexture(SDL_Renderer *rend, const std::string &file);
    void Render(SDL_Rect dest);
    void Render(int x, int y);
    int GetWidth();
    int GetHeight();
};

std::string getResourcePath();

#endif
