#include "button_text.h"

ButtonText::ButtonText()
{
}

ButtonText::ButtonText(int x, int y, int w, int h, const std::function<void()> &click, std::string text, Uint8 r, Uint8 g, Uint8 b)
{
    m_pos.x = x;
    m_pos.y = y;
    m_pos.w = w;
    m_pos.h = h;
    onClick = click;
    m_text = text;
    m_r = r;
    m_g = g;
    m_b = b;
    m_active = true;
}

ButtonText::~ButtonText()
{
}

void ButtonText::set(int x, int y, int w, int h, const std::function<void()> &click, std::string text, Uint8 r, Uint8 g, Uint8 b)
{
    m_pos.x = x;
    m_pos.y = y;
    m_pos.w = w;
    m_pos.h = h;
    onClick = click;
    m_text = text;
    m_r = r;
    m_g = g;
    m_b = b;
    m_active = true;
}

void ButtonText::show(GameEngine *ge) const
{
    if (!m_active)
        return;
    ge->fillRect(m_pos, m_r, m_g, m_b);
    ge->drawRect(m_pos, 0, 0, 0);
    ge->drawString(m_text, m_pos.x + 1, m_pos.y + 1);
}
