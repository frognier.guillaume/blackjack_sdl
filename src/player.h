#ifndef PLAYER_H
#define PLAYER_H
#include "card.h"
#include "shoe.h"
#include <vector>

class Player
{
public:
    Player();
    ~Player();

    unsigned int getNbCards() const;
    unsigned int getNbCardsHand() const;
    unsigned int getNbCardsSplit1() const;
    unsigned int getNbCardsSplit2() const;
    Card *getCard(unsigned int num) const;
    Card *getCardHand(unsigned int num) const;
    Card *getCardSplit1(unsigned int num) const;
    Card *getCardSplit2(unsigned int num) const;
    Card *getLastCardHand() const;
    void hit(Shoe *shoe);
    void reset();
    bool hasPair() const;
    void split(Shoe *shoe);
    bool hasSplit() const;
    bool hasSplit1done() const;
    void split1done();
    bool hasBust(short mph = -1) const;
    bool hasDouble() const;
    bool bet();
    bool halfBet();
    bool doubleW();
    // void increaseWager(unsigned int amount);
    // void decreaseWager(unsigned int amount);
    void increaseWager();
    void decreaseWager();
    void winWager(float coeff = 1);
    void setWager(unsigned int value);
    void setChips(float value);
    unsigned int getWager() const;
    float getChips() const;
    unsigned int total(short mph = -1) const;
    unsigned int softTotal(short mph = -1) const;
    std::string totalS(short mph = -1) const;
    bool hasTwentyOne() const;
    bool hasBlackJack() const;
    short getPlayingHand() const;
    void setPlayingHand(short n);

private:
    void calcTotal(Card *c, unsigned int &total, unsigned int &softTotal);

private:
    std::vector<Card *> m_hand;
    std::vector<Card *> m_split1;
    std::vector<Card *> m_split2;
    unsigned int m_wager;
    float m_chips;
    short m_playingHand;
    bool m_double;
    bool m_double1;
    bool m_double2;
    unsigned int m_total;
    unsigned int m_total1;
    unsigned int m_total2;
    unsigned int m_softTotal;
    unsigned int m_softTotal1;
    unsigned int m_softTotal2;
};

#endif
