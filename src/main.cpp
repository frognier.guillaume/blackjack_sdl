#include "game.h"
#include <iostream>

int main()
{
    Game game;
    game.initWindow(1500, 1000);
    game.initText();
    game.start();
    return 0;
}
