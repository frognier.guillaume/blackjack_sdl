#include "card.h"
#include <cmath>
using namespace std;

Card::Card()
{
    m_idcard = 0;
}

Card::Card(int idcard)
{
    m_idcard = idcard;
    this->setInfo();
}

Card::~Card()
{
}

void Card::setIdCard(unsigned int num)
{
    m_idcard = num;
    this->setInfo();
}

unsigned int Card::getIdCard() const
{
    return m_idcard;
}

string Card::getName() const
{
    return m_name;
}

string Card::getFullName() const
{
    return m_name + " of " + this->getSuitS();
}

string Card::getNick() const
{
    return m_nick;
}

string Card::getNickVC() const
{
    string str;
    str.append(m_nick);
    if (m_suit == 'h' || m_suit == 'd')
        str.push_back('r');
    else if (m_suit == 's' || m_suit == 'c')
        str.push_back('b');
    return str;
}

char Card::getSuitC() const
{
    return m_suit;
}

string Card::getSuitS() const
{
    string suit;
    switch (m_suit) {
    case ('h'):
        suit = "hearts";
        break;
    case ('s'):
        suit = "spades";
        break;
    case ('d'):
        suit = "diamonds";
        break;
    case ('c'):
        suit = "clubs";
        break;
    default:
        suit = "X";
        break;
    }
    return suit;
}

bool Card::isAs() const
{
    return (m_idcard % 13 == 1);
}

unsigned int Card::value() const
{
    return m_value;
}

unsigned int Card::lowValue() const
{
    return m_lowvalue;
}

void Card::setInfo()
{
    switch (m_idcard % 13) {
    case (1):
        m_name = "As";
        m_nick = "A";
        m_value = 11;
        m_lowvalue = 1;
        break;
    case (2):
        m_name = "two";
        m_nick = "2";
        m_value = 2;
        m_lowvalue = 0;
        break;
    case (3):
        m_name = "three";
        m_nick = "3";
        m_value = 3;
        m_lowvalue = 0;
        break;
    case (4):
        m_name = "four";
        m_nick = "4";
        m_value = 4;
        m_lowvalue = 0;
        break;
    case (5):
        m_name = "five";
        m_nick = "5";
        m_value = 5;
        m_lowvalue = 0;
        break;
    case (6):
        m_name = "six";
        m_nick = "6";
        m_value = 6;
        m_lowvalue = 0;
        break;
    case (7):
        m_name = "seven";
        m_nick = "7";
        m_value = 7;
        m_lowvalue = 0;
        break;
    case (8):
        m_name = "eight";
        m_nick = "8";
        m_value = 8;
        m_lowvalue = 0;
        break;
    case (9):
        m_name = "nine";
        m_nick = "9";
        m_value = 9;
        m_lowvalue = 0;
        break;
    case (10):
        m_name = "ten";
        m_nick = "10";
        m_value = 10;
        m_lowvalue = 0;
        break;
    case (11):
        m_name = "Jack";
        m_nick = "J";
        m_value = 10;
        m_lowvalue = 0;
        break;
    case (12):
        m_name = "Queen";
        m_nick = "Q";
        m_value = 10;
        m_lowvalue = 0;
        break;
    case (0):
        m_name = "King";
        m_nick = "K";
        m_value = 10;
        m_lowvalue = 0;
        break;
    default:
        // std::cerr << "Err: def class" << std::endl;
        break;
    }
    if (m_idcard >= 1 and m_idcard <= 13)
        m_suit = 'h';
    else if (m_idcard >= 14 and m_idcard <= 26)
        m_suit = 's';
    else if (m_idcard >= 27 and m_idcard <= 39)
        m_suit = 'd';
    else if (m_idcard >= 40 and m_idcard <= 52)
        m_suit = 'c';
    else
        m_suit = '/';
}
