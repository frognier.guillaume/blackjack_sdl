#include "shoe.h"
#include <cmath>
#include <time.h>
using namespace std;

Shoe::Shoe()
{
    m_ndeck = 6;
    m_rc = 0;
}

Shoe::~Shoe()
{
    this->clear();
}

void Shoe::addCard(unsigned int id)
{
    m_shoe.push_back(id); // debug
}

unsigned int Shoe::getNbCards() const
{
    return m_shoe.size();
}

unsigned int Shoe::at(unsigned int position) const
{
    return m_shoe.at(position);
}

void Shoe::clear()
{
    m_rc = 0;
    m_shoe.clear();
    return;
}

void Shoe::setNDeck(unsigned int ndeck)
{
    m_ndeck = ndeck;
}

void Shoe::increaseNdeck()
{
    m_ndeck++;
    if (m_ndeck > 16)
        m_ndeck = 16;
}

void Shoe::decreaseNdeck()
{
    m_ndeck--;
    if (m_ndeck < 1)
        m_ndeck = 1;
}

unsigned int Shoe::getNDeck() const
{
    return m_ndeck;
}

bool Shoe::half() const
{
    return m_shoe.size() < (float)(52 * m_ndeck) / (float)(2);
}

bool Shoe::quarter() const
{
    return m_shoe.size() < (float)(52 * m_ndeck) / (float)(4);
}

unsigned int Shoe::draw(bool count)
{
    if (m_shoe.empty())
        return -1;
    unsigned int idcard = m_shoe.back();
    m_shoe.pop_back();
    if (count)
        countRC(idcard);
    else
        m_id_face_down = idcard;
    return idcard;
}

void Shoe::countRC(unsigned int idcard)
{
    unsigned int valueidcard = idcard % 13;
    if (valueidcard == 0 || valueidcard == 1 || (valueidcard >= 10 && valueidcard <= 12))
        // K,A,10,J,Q
        m_rc--;
    else if (valueidcard >= 2 && valueidcard <= 6)
        m_rc++;
}

void Shoe::countFaceDown()
{
    countRC(m_id_face_down);
}

void Shoe::init()
{
    for (unsigned int k = 1; k <= m_ndeck; k++) {
        for (unsigned int i = 1; i <= 52; i++) {
            m_shoe.push_back(i);
        }
    }
}

void Shoe::shuffle(unsigned int superseed)
{
    // get all deck's cardId in a vector.
    // clear the deck.
    std::vector<unsigned int> idcardsdeck;
    while (m_shoe.empty() == false) {
        idcardsdeck.push_back(m_shoe.back());
        m_shoe.pop_back();
    }
    // create a vector with random values between 0 and nbcardsd (no same values).
    // add a new card with random cardId in order to fill the deck again.
    vector<unsigned int> randint;
    bool present;
    srand48(time(NULL) + superseed);
    while (randint.size() < idcardsdeck.size()) {
        present = false;
        unsigned int rn = floor(drand48() * (idcardsdeck.size()));
        for (unsigned int i = 0; i < randint.size(); i++) {
            if (rn == randint.at(i)) {
                present = true;
                break;
            }
        }
        if (not present) {
            randint.push_back(rn);
            m_shoe.push_back(idcardsdeck.at(rn));
        }
    }
}

short Shoe::rc() const
{ // running count
    return m_rc;
}

float Shoe::tc() const
{ // true count
    return (float)(m_rc) / std::ceil((float)(m_shoe.size()) / 52);
}
