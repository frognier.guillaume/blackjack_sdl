#ifndef CARD_H
#define CARD_H
#include <string>

class Card
{
public:
    Card();
    Card(int idcard);
    ~Card();
    void setIdCard(unsigned int num);
    unsigned int getIdCard() const;
    std::string getName() const;
    std::string getFullName() const;
    char getSuitC() const;
    std::string getSuitS() const;
    std::string getNick() const;
    std::string getNickVC() const;
    bool isAs() const;
    unsigned int value() const;
    unsigned int lowValue() const;

private:
    void setInfo();

private:
    unsigned int m_idcard;
    std::string m_name;
    std::string m_nick;
    char m_suit;
    unsigned int m_value;
    unsigned int m_lowvalue;
};

#endif
