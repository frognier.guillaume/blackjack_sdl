#include "game.h"

Game::Game()
{
    m_sAppName = "BlackJack";
}

Game::~Game()
{
}

bool Game::onUserCreate()
{
    loadTexturesFromDisk("2r", "red-2.bmp");
    loadTexturesFromDisk("3r", "red-3.bmp");
    loadTexturesFromDisk("4r", "red-4.bmp");
    loadTexturesFromDisk("5r", "red-5.bmp");
    loadTexturesFromDisk("6r", "red-6.bmp");
    loadTexturesFromDisk("7r", "red-7.bmp");
    loadTexturesFromDisk("8r", "red-8.bmp");
    loadTexturesFromDisk("9r", "red-9.bmp");
    loadTexturesFromDisk("10r", "red-10.bmp");
    loadTexturesFromDisk("Jr", "red-J.bmp");
    loadTexturesFromDisk("Qr", "red-Q.bmp");
    loadTexturesFromDisk("Kr", "red-K.bmp");
    loadTexturesFromDisk("Ar", "red-A.bmp");
    loadTexturesFromDisk("2b", "black-2.bmp");
    loadTexturesFromDisk("3b", "black-3.bmp");
    loadTexturesFromDisk("4b", "black-4.bmp");
    loadTexturesFromDisk("5b", "black-5.bmp");
    loadTexturesFromDisk("6b", "black-6.bmp");
    loadTexturesFromDisk("7b", "black-7.bmp");
    loadTexturesFromDisk("8b", "black-8.bmp");
    loadTexturesFromDisk("9b", "black-9.bmp");
    loadTexturesFromDisk("10b", "black-10.bmp");
    loadTexturesFromDisk("Jb", "black-J.bmp");
    loadTexturesFromDisk("Qb", "black-Q.bmp");
    loadTexturesFromDisk("Kb", "black-K.bmp");
    loadTexturesFromDisk("Ab", "black-A.bmp");
    loadTexturesFromDisk("c", "club.bmp");
    loadTexturesFromDisk("h", "heart.bmp");
    loadTexturesFromDisk("s", "spade.bmp");
    loadTexturesFromDisk("d", "diamond.bmp");
    setBackGroundColor(30, 90, 50);
    m_delay = 100;
    m_GameState = GS_WAITING_BET;
    m_round = 1;
    m_stat = false;
    // initialization shoe
    shoe.init();
    shoe.shuffle();
    /*
   shoe.addCard(8); //other hit
   shoe.addCard(6); //other hit
   shoe.addCard(16); //other hit
   shoe.addCard(2); //other hit
   shoe.addCard(1); //dealer's face down card
   shoe.addCard(1); //as //2nd card p
   shoe.addCard(1); //1st card dealer
   shoe.addCard(14); //as //1st card p
   // */
    // initialization buttons
    m_button_bet.set(100, windowHeight() - 100, 100, 50, std::bind(&Game::bet, this), "bet", GREEN.r, GREEN.g, GREEN.b);
    m_button_bet_inc.set(250, windowHeight() - 100, 100, 50, std::bind(&Player::increaseWager, &player), "bet+5", ORANGE.r, ORANGE.g, ORANGE.b);
    m_button_bet_dec.set(400, windowHeight() - 100, 100, 50, std::bind(&Player::decreaseWager, &player), "bet-5", ORANGE.r, ORANGE.g, ORANGE.b);
    m_button_option.set(windowWidth() - 70, 20, 50, 50, std::bind(&Game::option, this), "opt", GREY.r, GREY.g, GREY.b);
    m_button_hit.set(100, windowHeight() - 100, 100, 50, std::bind(&Game::hit, this), "hit", GREEN.r, GREEN.g, GREEN.b);
    m_button_stand.set(250, windowHeight() - 100, 100, 50, std::bind(&Game::stand, this), "stand", RED.r, RED.g, RED.b);
    m_button_double.set(400, windowHeight() - 100, 100, 50, std::bind(&Game::doubleW, this), "double", YELLOW.r, YELLOW.g, YELLOW.b);
    m_button_split.set(550, windowHeight() - 100, 100, 50, std::bind(&Game::split, this), "split", BLUE.r, BLUE.g, BLUE.b);
    m_button_reset.set(100, windowHeight() - 100, 100, 50, std::bind(&Game::reset, this), "reset", YELLOW.r, YELLOW.g, YELLOW.b);
    m_button_quit.set(250, windowHeight() - 100, 100, 50, std::bind(&Game::quit, this), "quit", GREY.r, GREY.g, GREY.b);
    m_button_shoe_inc.set(500, 15, 20, 20, std::bind(&Shoe::increaseNdeck, &shoe), "+", GREY.r, GREY.g, GREY.b);
    m_button_shoe_dec.set(560, 15, 20, 20, std::bind(&Shoe::decreaseNdeck, &shoe), "-", GREY.r, GREY.g, GREY.b);
    m_button_count.set(500, 40, 20, 20, std::bind(&Game::turnStat, this), false);
    m_button_reset_all.set(500, 65, 20, 20, std::bind(&Game::resetAll, this), GREY.r, GREY.g, GREY.b);
    m_button_insuranceY.set(100, windowHeight() - 100, 100, 50, std::bind(&Game::insuranceYes, this), "Yes", GREEN.r, GREEN.g, GREEN.b);
    m_button_insuranceN.set(250, windowHeight() - 100, 100, 50, std::bind(&Game::insuranceNo, this), "No", RED.r, RED.g, RED.b);
    // m_button_auto.set(700, windowHeight()-100, 100, 50, std::bind(&Game::insuranceNo, this), "auto", DARK_RED.r, DARK_RED.g, DARK_RED.b);
    m_button_split.setInactive();
    show();
    return true;
}

void Game::bet()
{
    if (!player.bet()) {
        addPrint("Not enough cash.");
        return;
    }
    player.hit(&shoe);
    dealer.hit(&shoe);
    player.hit(&shoe);
    dealer.hit(&shoe, false);
    m_GameState = GS_DEALING;
    if (player.hasPair())
        m_button_split.setActive();
    if (dealer.getFirstCard()->getNick() == "A") {
        addPrint("Insurance?");
        addPrint("(bet " + to_string_c((float)player.getWager() / 2) + ")");
        m_GameState = GS_ASK_INSURANCE;
    } else if (player.hasBlackJack()) {
        addPrint("BlackJack!");
        end();
    } else if (dealer.getFirstCard()->value() == 10) {
        addPrint("Checking for blackjack...");
        if (dealer.hasBlackJack()) {
            addPrint("Blackjack.");
            end();
        } else
            addPrint("Dealer doesn't have blackjack.");
    }
}

void Game::stand()
{
    if (player.hasSplit() && !player.hasSplit1done()) {
        player.split1done();
        m_button_double.setActive();
        if (player.hasTwentyOne()) {
            addPrint("Twenty-one!");
            stand();
        }
        return;
    } else {
        dealer.seventeen(&shoe);
        if (dealer.hasBust()) // dealer busts
            addPrint("Dealer busts");
        end();
    }
}

void Game::hit()
{
    m_button_split.setInactive();
    m_button_double.setInactive();
    player.hit(&shoe);
    if ((player).hasBust()) {
        addPrint("You bust");
        if (player.hasSplit()) {
            if (player.hasSplit1done()) { // split2 ongoing then bust
                if (player.hasBust(1)) {
                    end(); // bust&bust=dealer win without buying cards
                    return;
                } else {
                    stand();
                    return;
                }
            } else { // split1 ongoing then bust
                stand();
                return;
            }
        } else // no split
            end();
    } else if (player.hasTwentyOne()) {
        addPrint("Twenty-one!");
        stand();
    }
}

void Game::doubleW()
{
    m_button_double.setInactive();
    if (!player.doubleW()) {
        addPrint("Not enough cash.");
        return;
    }
    player.hit(&shoe);
    if ((player).hasBust()) {
        addPrint("You bust");
        if (player.hasSplit()) {
            if (player.hasSplit1done()) { // split2 ongoing then bust
                if (player.hasBust(1)) {
                    end(); // bust&bust=dealer win without buying cards
                    return;
                } else {
                    stand();
                    return;
                }
            } else { // split1 ongoing then bust
                stand();
                return;
            }
        } else // no split
            end();
    } else if (player.hasTwentyOne()) {
        addPrint("Twenty-one!");
        stand();
    } else
        stand();
}

void Game::split()
{
    // m_button_double.setInactive();
    m_button_split.setInactive();
    if (!player.bet()) {
        addPrint("Not enough cash.");
        return;
    }
    player.split(&shoe);
    if (player.hasTwentyOne()) {
        addPrint("Twenty-one!");
        stand();
    }
    if (player.getCard(0)->isAs()) {
        stand(); // only one more card for split aces
        stand();
    }
}

void Game::end()
{
    dealer.flip();
    shoe.countFaceDown();
    if (player.hasBlackJack() && !dealer.hasBlackJack()) { // win blackjack
        float coeff = 2.5;
        player.winWager(coeff);
        addPrint("Win! (+" + to_string_c(player.getWager() * coeff) + ")");
        // showOnStacksEnd(player.getWager(),coeff,false);
    } else if (player.hasBust() || (!dealer.hasBust() && player.total() < dealer.total())) {
        // lose + lose when p busts first (split) + (p21 vs dbj = losing but not possible)
        // or (not(*actp).hasBlackJack() and (*dealer).hasBlackJack())){//lose
        addPrint("No win.");
        //		showOnStacksEnd(player.getWager()*(player.hasDouble()+1),0.0f,player.hasSplit1done());
    } else if (player.total() > dealer.total() || dealer.hasBust()) { // win
        unsigned int coeff = 2;
        if (player.hasDouble())
            coeff *= 2;
        player.winWager(coeff);
        addPrint("Win! (+" + std::to_string(player.getWager() * coeff) + ")");
        //		showOnStacksEnd(player.getWager()*(player.hasDouble()+1),2.0f,player.hasSplit1done());
    } else { // tie
        unsigned int coeff = 1;
        if (player.hasDouble())
            coeff *= 2;
        player.winWager(coeff);
        addPrint("Push. (+" + std::to_string(player.getWager() * coeff) + ")");
        //		showOnStacksEnd(player.getWager()*(player.hasDouble()+1),1.0f,player.hasSplit1done());
    }
    if (player.hasSplit() && player.hasSplit1done()) {
        addPrint("Then");
        player.setPlayingHand(1);
        end();
    }
    m_GameState = GS_ASK_RESET;
}

void Game::reset()
{
    m_round++;
    m_button_split.setInactive();
    m_button_double.setActive();
    (player).reset();
    dealer.reset();
    if (shoe.quarter()) {
        shoe.clear();
        shoe.init();
        shoe.shuffle();
        addPrint("New Deck.");
    }
    m_GameState = GS_WAITING_BET;
}

bool Game::onUserUpdate(float fElapsedTime)
{
    // std::cout << "Game::onUserUpdate(" << fElapsedTime << ")" << std::endl;
    handleEvents();
    SDL_Delay(m_delay);
    return true;
}

void Game::show()
{
    // std::cout << "Game::show()" << std::endl;
    clearScreen();
    switch (m_GameState) {
    case (GS_WAITING_BET): // waiting for bets
        m_button_bet.show(this);
        m_button_bet_inc.show(this);
        m_button_bet_dec.show(this);
        m_button_option.show(this);
        break;
    case (GS_DEALING): // main dealing
        m_button_hit.show(this);
        m_button_stand.show(this);
        m_button_double.show(this);
        m_button_split.show(this);
        break;
    case (GS_ASK_RESET): // 0 stand by at the end
        m_button_reset.show(this);
        m_button_quit.show(this);
        break;
    case (GS_OPTION):
        m_button_option.show(this);
        m_button_shoe_inc.show(this);
        m_button_shoe_dec.show(this);
        m_button_count.show(this);
        m_button_reset_all.show(this);
        break;
    case (GS_ASK_INSURANCE):
        m_button_insuranceY.show(this);
        m_button_insuranceN.show(this);
        break;
    default:
        break;
    }
    if (m_GameState != GS_OPTION) {
        SDL_Rect box_dealer = {100, 20, 100, 17};
        SDL_Rect box_player = {100, 350, 100, 17};
        fillRect(box_player, WHITE);
        fillRect(box_dealer, WHITE);
        drawString("Player:", box_player.x + 1, box_player.y + 1);
        drawString("Dealer:", box_dealer.x + 1, box_dealer.y + 1);
        SDL_Rect box_card_dealer = {100, 50, 130, 200};
        SDL_Rect box_card_player = {100, 380, 130, 200};
        static const SDL_Rect box_total_dealer = {100, 260, 110, 20};
        fillRect(box_total_dealer, WHITE);
        drawString(dealer.totalS(), box_total_dealer.x + 1, box_total_dealer.y + 1);
        for (unsigned int i = 0; i < dealer.getNbCards(); i++) {
            fillRect(box_card_dealer, WHITE);
            if (i != 1 || dealer.isShowing()) {
                m_texmap.GetID(dealer.getCard(i)->getNickVC())->Render(box_card_dealer.x, box_card_dealer.y);
                m_texmap.GetID(std::string(1, dealer.getCard(i)->getSuitC()))->Render(box_card_dealer.x, box_card_dealer.y + 50);
            }
            box_card_dealer.x += 140;
        }
        if (!player.hasSplit()) {
            static const SDL_Rect box_total_player = {100, 590, 110, 20};
            fillRect(box_total_player, WHITE);
            drawString(player.totalS(), box_total_player.x + 1, box_total_player.y + 1);
            for (unsigned int i = 0; i < player.getNbCards(); i++) {
                fillRect(box_card_player, WHITE);
                // drawString(player.getCardHand(i)->getName(), box_card_player.x+5, box_card_player.y+20);
                m_texmap.GetID(player.getCard(i)->getNickVC())->Render(box_card_player.x, box_card_player.y);
                m_texmap.GetID(std::string(1, player.getCard(i)->getSuitC()))->Render(box_card_player.x, box_card_player.y + 50);
                box_card_player.x += 140;
            }
        } else {
            static const SDL_Rect box_total1_player = {100, 590, 110, 20};
            static const SDL_Rect box_total2_player = {1000, 590, 110, 20};
            int cursorx = (player.getPlayingHand() == 1) ? 220 : 1120;
            SDL_Rect box_cursor = {cursorx, 590, 20, 20};
            fillRect(box_cursor, YELLOW);
            fillRect(box_total1_player, WHITE);
            fillRect(box_total2_player, WHITE);
            drawString(player.totalS(1), box_total1_player.x + 1, box_total1_player.y + 1);
            drawString(player.totalS(2), box_total2_player.x + 1, box_total2_player.y + 1);
            for (unsigned int i = 0; i < player.getNbCardsSplit1(); i++) {
                fillRect(box_card_player, WHITE);
                m_texmap.GetID(player.getCardSplit1(i)->getNickVC())->Render(box_card_player.x, box_card_player.y);
                m_texmap.GetID(std::string(1, player.getCardSplit1(i)->getSuitC()))->Render(box_card_player.x, box_card_player.y + 50);
                box_card_player.x += 50;
            }
            box_card_player.x = 1000;
            for (unsigned int i = 0; i < player.getNbCardsSplit2(); i++) {
                fillRect(box_card_player, WHITE);
                m_texmap.GetID(player.getCardSplit2(i)->getNickVC())->Render(box_card_player.x, box_card_player.y);
                m_texmap.GetID(std::string(1, player.getCardSplit2(i)->getSuitC()))->Render(box_card_player.x, box_card_player.y + 50);
                box_card_player.x += 50;
            }
        }
    } else { // option
        drawString("Decks in the shoe: " + std::to_string(shoe.getNDeck()), 100, 15);
        drawString("Display statistics:", 100, 40);
        drawString("Reset all:", 100, 65);
    }
    showPrint();
    //	showOnStacks();
    // render();
}

void Game::handleEvents()
{
    SDL_Event event;
    bool needshow = false;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_WINDOWEVENT:
            windowResize();
            needshow = true;
            break;
        case SDL_QUIT:
            quit();
            break;
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym) {
            case SDLK_ESCAPE: // Shut down
                quit();
                break;
            case SDLK_RETURN:
                break;
            default:
                break;
            }
            break;
        case SDL_MOUSEBUTTONDOWN:
            if (event.button.button == SDL_BUTTON_LEFT) {
                int x = event.button.x;
                int y = event.button.y;
                switch (m_GameState) {
                case (GS_WAITING_BET):
                    needshow += m_button_bet.clickAction(x, y);
                    needshow += m_button_bet_inc.clickAction(x, y);
                    needshow += m_button_bet_dec.clickAction(x, y);
                    needshow += m_button_option.clickAction(x, y);
                    break;
                case (GS_DEALING):
                    needshow += m_button_hit.clickAction(x, y);
                    needshow += m_button_stand.clickAction(x, y);
                    needshow += m_button_double.clickAction(x, y);
                    needshow += m_button_split.clickAction(x, y);
                    break;
                case (GS_ASK_RESET):
                    needshow += m_button_reset.clickAction(x, y);
                    needshow += m_button_quit.clickAction(x, y);
                    break;
                case (GS_OPTION):
                    needshow += m_button_shoe_inc.clickAction(x, y);
                    needshow += m_button_shoe_dec.clickAction(x, y);
                    needshow += m_button_option.clickAction(x, y);
                    needshow += m_button_count.clickAction(x, y);
                    needshow += m_button_reset_all.clickAction(x, y);
                    break;
                case (GS_ASK_INSURANCE):
                    needshow += m_button_insuranceY.clickAction(x, y);
                    needshow += m_button_insuranceN.clickAction(x, y);
                    break;
                default:
                    break;
                }
            }
            break;
        default:
            break;
        }
    }
    if (needshow)
        show();
}

void Game::option()
{
    if (m_GameState == GS_WAITING_BET)
        m_GameState = GS_OPTION;
    else
        m_GameState = GS_WAITING_BET;
}

void Game::turnStat()
{
    m_stat = !m_stat;
}

void Game::insuranceYes()
{
    m_GameState = GS_DEALING;
    if (dealer.hasBlackJack()) {
        addPrint("Insurance won! (+" + std::to_string(player.getWager()) + ")");
        player.winWager();
        end();
        return;
    } else {
        addPrint("Insurance lost.");
        player.halfBet();
        if (player.hasBlackJack())
            end();
        return;
    }
}

void Game::insuranceNo()
{
    m_GameState = GS_DEALING;
    if (dealer.hasBlackJack()) {
        addPrint("Dealer had BlackJack.");
        end();
        return;
    } else {
        addPrint("Dealer doesn't have BlackJack.");
        if (player.hasBlackJack()) {
            addPrint("But you do!");
            end();
        }
        return;
    }
}

void Game::addPrint(std::string line)
{
    m_print.push_back(line);
}

void Game::resetAll()
{
    m_round = 0;
    m_button_split.setInactive();
    m_button_double.setActive();
    player.reset();
    dealer.reset();
    player.setWager(10);
    player.setChips(10000);
    shoe.clear();
    shoe.init();
    shoe.shuffle();
    addPrint("All reset");
}

/*
void Game::showCard(){
}

void Game::showOnStacksEnd(unsigned int total_bet, float coeff, bool box2){
    int x = 110;
    int y = 290;
    if (box2)
        x=1100;
    drawString("bet "+std::to_string(total_bet), x, y-20);
    if (coeff==0.0f)
        drawString("No win.",x,y);
    else if (coeff==1.0f)
        drawString("Push (+"+std::to_string(total_bet)+")",x,y);
    else if (coeff==2.0f)
        drawString("Win (+"+to_string_c(total_bet*coeff)+")",x,y);
    else if (coeff==2.5f)
        drawString("Win (+"+to_string_c(total_bet*coeff)+")",x,y);
}

void Game::showOnStacks(){
    if (player.hasBlackJack()){
        int x = 110;
        int y_player = 310;
        drawString("BlackJack!",x,y_player);
    }
    if (dealer.hasBlackJack()){
        int x = 110;
        int y_dealer = 170;
        drawString("BlackJack!",x,y_dealer);
    }
}
// */

void Game::showPrint()
{
    if (m_stat) {
        static const SDL_Rect box_stat = {700, windowHeight() - 350, 500, 80};
        fillRect(box_stat, WHITE);
        drawString("Cards left:    " + std::to_string(shoe.getNbCards()), box_stat.x + 1, box_stat.y + 1);
        drawString("Running Count: " + std::to_string(shoe.rc()), box_stat.x + 1, box_stat.y + 17);
        drawString("True Count:    " + to_string_p(shoe.tc()), box_stat.x + 1, box_stat.y + 33);
        if (m_GameState == GS_DEALING) {
            std::string strhint;
            switch (basic_strategy(player.hasPair(), player.total(), player.softTotal(), dealer.getFirstCard()->value())) {
            case ('s'):
                strhint = "stand";
                break;
            case ('h'):
                strhint = "hit";
                break;
            case ('p'): // split/hit
                strhint = player.hasSplit() ? "hit" : "split";
                break;
            case ('d'): // double/hit
                strhint = player.getNbCards() != 2 ? "hit" : "double";
                break;
            case ('1'): // double/stand
                strhint = player.getNbCards() != 2 ? "stand" : "double";
                break;
            default:
                strhint = "unknown";
                break;
            }
            drawString("Hint:          " + strhint, box_stat.x + 1, box_stat.y + 49);
        }
    }
    static const SDL_Rect box_cash = {100, windowHeight() - 350, 500, 40};
    static const SDL_Rect box_print = {100, windowHeight() - 300, 500, 120};
    fillRect(box_print, WHITE);
    fillRect(box_cash, WHITE);
    drawString("balance: " + to_string_c(player.getChips()), box_cash.x + 1, box_cash.y + 1);
    drawString("wager:   " + std::to_string(player.getWager()), box_cash.x + 1, box_cash.y + 17);
    for (unsigned int i = 0; i < m_print.size(); i++) {
        drawString(m_print.at(i), box_print.x + 1, box_print.y + 1 + i * 16);
    }
    m_print.clear();
}

std::string to_string_p(float f, unsigned int precision)
{
    std::ostringstream streamObj;
    // Set Fixed -Point Notation
    streamObj << std::fixed;
    // Set precision to 2 digits
    streamObj << std::setprecision(precision);
    // Add double to stream
    streamObj << f;
    // Get string from output string stream
    return streamObj.str();
}

std::string to_string_c(float f)
{
    std::string str = std::to_string(f);
    size_t found = str.find('.');
    if (found != std::string::npos) {
        found = str.find('0', found);
        if (found != std::string::npos) {
            str.resize(found);
            if (str.back() == '.')
                str.pop_back();
        }
    }
    return str;
}

/*
char basic_strategy(bool pair, unsigned int p, unsigned int sp, unsigned int d){
  if (pair){ //pairs
    switch(p){
      case(20):
        return 's'; //stand
      case(18):
        if (d==7 or d>=10)
          return 's';
        else if (d<=6 or d==8 or d==9)
          return 'p'; //split
        else
          return 'x';
      case(16):
        return 'p';
      case(14):
        if (d<=7)
          return 'p';
        else if (d>=8)
          return 'h'; //hit
        else
          return 'x';
      case(12):
        if (p!=sp) //As
          return 'p';
        if (d<=6) //6
          return 'p';
        else if (d>=7)
          return 'h';
        else
          return 'x';
      case(10):
        if (d<=9)
          return 'd'; //2:double/hit
        else if (d>=10)
          return 'h';
        else
          return 'x';
      case(8):
        if (d==5 or d==6)
          return 'p';
        else if (d<=4 or d>=7)
          return 'h';
        else
          return 'x'; //trouble
      case(6):
      case(4):
        if (d<=7)
          return 'p';
        else if (d>=8)
          return 'h';
        else
          return 'x'; //trouble
      default:
        return 'x'; //trouble
    }
  }
  else if (sp==p){ //Hard totals excluding pairs
    switch(p){
      case(20):
      case(19):
      case(18):
      case(17):
        return 's';
      case(16):
      case(15):
      case(14):
      case(13):
        if (d<=6)
          return 's';
        else if (d>=7)
          return 'h';
        else
          return 'x';
      case(12):
        if (d<=3 or d>=7)
          return 'h';
        else if (d>=4 and d<=6)
          return 's';
        else
          return 'x';
      case(11):
        return 'd'; //2:double/hit
      case(10):
        if (d<=9)
          return 'd'; //2:double/hit
        else if (d>=10)
          return 'h';
        else
          return 'x';
      case(9):
        if (d==2 or d>=7)
          return 'h';
        else if (d>=3 and d<=6)
          return 'd'; //2:double/hit
        else
          return 'x';
      case(8):
      case(7):
      case(6):
      case(5):
      case(4):
      case(3):
      case(2):
        return 'h';
      default:
        return 'x'; //trouble
    }
  }
    else{ //Soft totals
    //std::cout << "Soft totals (TRM)" << std::endl;
    switch(sp){
      case(10):
      //  std::cout << "case10 (TRM)" << std::endl;
        return 's';
      case(9):
        if (d!=6)
          return 's';
        else if (d==6)
          return '1';//1:double/stand
        else
          return 'x';
      case(8):
        if (d==7 or d==8)
          return 's';
        else if (d>=9)
          return 'h';
        else if (d<=6)
          return '1';//1:double/stand
        else
          return 'x';
      case(7):
        if (d==2 or d>=7)
          return 'h';
        else if (d!=2 and d<=6)
          return 'd'; //2:double/hit
        else
          return 'x';
      case(6):
      case(5):
        if (d<=3 or d>=7)
          return 'h';
        else if (d>=4 and d<=6)
          return 'd'; //2:double/hit
        else
          return 'x';
      case(4):
      case(3):
        //std::cout << "sp=4or3 (TRM)" << std::endl;
        if (d<=4 or d>=7)
          //std::cout << "d<=4 or d>=7 (TRM)" << std::endl;
          return 'h';
        else if (d==5 or d==6)
          return 'd'; //2:double/hit
        else
          return 'x';
      default:
        std::cout << "default (TRM)" << std::endl;
        return 'x';
    }
  }
}

// */

char basic_strategy(bool pair, unsigned int p, unsigned int sp, unsigned int d)
{
    if (pair) { // pair split
        static char c_pair[10][10] = {
            {'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'}, // A,A
            {'p', 'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'}, // 2,2
            {'p', 'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'}, // 3,3
            {'h', 'h', 'h', 'p', 'p', 'h', 'h', 'h', 'h', 'h'}, // 4,4
            {'d', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'h', 'h'}, // 5,5
            {'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h', 'h'}, // 6,6
            {'p', 'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'}, // 7,7
            {'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'}, // 8,8
            {'p', 'p', 'p', 'p', 'p', 's', 'p', 'p', 's', 's'}, // 9,9
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'} // 10,10
        };
        return c_pair[sp / 2 - 1][d - 2];
    } else if (p == sp) { // hard total excluding pair
        static char c_hard[16][10] = {
            {'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'}, // 5
            {'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'}, // 6
            {'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'}, // 7
            {'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'}, // 8
            {'h', 'd', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'}, // 9
            {'d', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'h', 'h'}, // 10
            {'d', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd'}, // 11
            {'h', 'h', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'}, // 12
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'}, // 13
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'}, // 14
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'}, // 15
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'}, // 16
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}, // 17
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}, // 18
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}, // 19
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'} // 20
        };
        return c_hard[p - 5][d - 2];
    } else { // soft total excluding pair
        static char c_soft[8][10] = {
            {'h', 'h', 'h', 'd', 'd', 'h', 'h', 'h', 'h', 'h'}, // A,2
            {'h', 'h', 'h', 'd', 'd', 'h', 'h', 'h', 'h', 'h'}, // A,3
            {'h', 'h', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'}, // A,4
            {'h', 'h', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'}, // A,5
            {'h', 'd', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'}, // A,6
            {'1', '1', '1', '1', '1', 's', 's', 'h', 'h', 'h'}, // A,7
            {'s', 's', 's', 's', '1', 's', 's', 's', 's', 's'}, // A,8
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}, // A,9
        };
        return c_soft[sp - 3][d - 2];
    }
}
