#ifndef GAME_H
#define GAME_H

#include "button_check.h"
#include "button_mere.h"
#include "button_text.h"
#include "dealer.h"
#include "gameengine.h"
#include "player.h"
#include "shoe.h"
#include <iomanip>
#include <sstream>
#include <vector>

class Game : public GameEngine
{
public:
    Game();
    ~Game();

private:
    unsigned int m_delay;
    enum GAME_STATE { GS_WAITING_BET, GS_DEALING, GS_ASK_RESET, GS_OPTION, GS_ASK_INSURANCE } m_GameState;
    unsigned int m_round;
    bool m_stat;
    // unsigned int m_step;
    Shoe shoe;
    Dealer dealer;
    Player player;
    ButtonText m_button_bet;
    ButtonText m_button_bet_inc;
    ButtonText m_button_bet_dec;
    ButtonText m_button_option;
    ButtonText m_button_hit;
    ButtonText m_button_stand;
    ButtonText m_button_double;
    ButtonText m_button_split;
    ButtonText m_button_reset;
    ButtonText m_button_quit;
    ButtonText m_button_shoe_inc;
    ButtonText m_button_shoe_dec;
    ButtonText m_button_insuranceY;
    ButtonText m_button_insuranceN;
    ButtonCheck m_button_count;
    ButtonMere m_button_reset_all;
    // ButtonText m_button_auto;

    std::vector<std::string> m_print;

protected:
    virtual bool onUserCreate();
    virtual bool onUserUpdate(float fElapsedTime);
    virtual void handleEvents();
    void show();
    void addPrint(std::string line);
    void showPrint();
    // void showCard();
    // void showOnStacksEnd(unsigned int total_bet, float coeff, bool box2);
    // void showOnStacks();

    void bet();
    void doubleW();
    void stand();
    void hit();
    void split();
    void end();
    void reset();
    void insuranceYes();
    void insuranceNo();

    void resetAll();
    void option();
    void turnStat();
    // void followStat();
};

std::string to_string_p(float f, unsigned int precision = 2);
std::string to_string_c(float f);
char basic_strategy(bool pair, unsigned int sp, unsigned int p, unsigned int d);

#endif
