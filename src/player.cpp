#include "player.h"
using namespace std;

Player::Player()
{
    m_wager = 10;
    m_chips = 10000;
    m_playingHand = 0;
    m_double = false;
    m_double1 = false;
    m_double2 = false;
    m_total = 0;
    m_total1 = 0;
    m_total2 = 0;
    m_softTotal = 0;
    m_softTotal1 = 0;
    m_softTotal2 = 0;
}

Player::~Player()
{
    this->reset();
}

unsigned int Player::getNbCards() const
{
    switch (m_playingHand) {
    case (0):
        return m_hand.size();
    case (1):
        return m_split1.size();
    case (2):
        return m_split2.size();
    default:
        return 0;
    }
}

unsigned int Player::getNbCardsHand() const
{
    return m_hand.size();
}

unsigned int Player::getNbCardsSplit1() const
{
    return m_split1.size();
}

unsigned int Player::getNbCardsSplit2() const
{
    return m_split2.size();
}

void Player::reset()
{
    while (!m_hand.empty()) {
        delete m_hand.back();
        m_hand.pop_back();
    }
    while (!m_split1.empty()) {
        delete m_split1.back();
        m_split1.pop_back();
    }
    while (!m_split2.empty()) {
        delete m_split2.back();
        m_split2.pop_back();
    }
    m_playingHand = 0;
    m_double = false;
    m_double1 = false;
    m_double2 = false;
    m_total = 0;
    m_total1 = 0;
    m_total2 = 0;
    m_softTotal = 0;
    m_softTotal1 = 0;
    m_softTotal2 = 0;
    return;
}

Card *Player::getCardHand(unsigned int num) const
{
    return m_hand[num]; // list begin by 0!
}

Card *Player::getCard(unsigned int num) const
{
    switch (m_playingHand) {
    case (0):
        return m_hand[num]; // list begin by 0!
    case (1):
        return m_split1[num];
    case (2):
        return m_split2[num];
    default:
        return nullptr;
    }
}

Card *Player::getCardSplit1(unsigned int num) const
{
    return m_split1[num]; // list begin by 0!
}

Card *Player::getCardSplit2(unsigned int num) const
{
    return m_split2[num]; // list begin by 0!
}

Card *Player::getLastCardHand() const
{
    return m_hand.back();
}

void Player::hit(Shoe *shoe)
{
    switch (m_playingHand) {
    case 0:
        m_hand.push_back(new Card);
        m_hand.back()->setIdCard(shoe->draw());
        this->calcTotal(m_hand.back(), m_total, m_softTotal);
        return;
    case 1:
        m_split1.push_back(new Card);
        m_split1.back()->setIdCard(shoe->draw());
        this->calcTotal(m_split1.back(), m_total1, m_softTotal1);
        return;
    case 2:
        m_split2.push_back(new Card);
        m_split2.back()->setIdCard(shoe->draw());
        this->calcTotal(m_split2.back(), m_total2, m_softTotal2);
        return;
    default:
        return;
    }
}

void Player::calcTotal(Card *c, unsigned int &total, unsigned int &softTotal)
{
    if (c->isAs()) {
        softTotal += 1;
        total += 11;
        if (total > 21)
            total -= 10;
    } else {
        softTotal += c->value();
        total += c->value();
        if (total > 21)
            total = softTotal;
    }
}

void Player::setChips(float value)
{
    m_chips = value;
}

float Player::getChips() const
{
    return m_chips;
}

void Player::setWager(unsigned int value)
{
    m_wager = value;
}

/*
void Player::increaseWager(unsigned int amount){
    m_wager+=amount;
}

void Player::decreaseWager(unsigned int amount){
    m_wager-=amount;
}
*/

void Player::increaseWager()
{
    m_wager += 5;
}

void Player::decreaseWager()
{
    m_wager -= 5;
    if (m_wager < 1)
        m_wager = 5;
}

unsigned int Player::getWager() const
{
    return m_wager;
}

void Player::winWager(float coeff)
{
    m_chips += m_wager * coeff;
}

bool Player::bet()
{
    if (m_wager > m_chips)
        return false;
    m_chips -= m_wager;
    return true;
}

bool Player::halfBet()
{
    float halfwager = (float)m_wager / 2;
    if (halfwager > m_chips)
        return false;
    m_chips -= halfwager;
    return true;
}

bool Player::doubleW()
{
    if (!this->bet())
        return false;
    switch (m_playingHand) {
    case (0):
        m_double = true;
        return true;
    case (1):
        m_double1 = true;
        return true;
    case (2):
        m_double2 = true;
        return true;
    default:
        return false;
        break;
    }
}

bool Player::hasDouble() const
{
    switch (m_playingHand) {
    case (0):
        return m_double;
    case (1):
        return m_double1;
    case (2):
        return m_double2;
    default:
        return false;
    }
}

bool Player::hasBust(short mph) const
{
    return (this->total(mph) > 21);
}

bool Player::hasPair() const
{
    return (m_hand.size() == 2 && m_hand.front()->value() == m_hand.back()->value());
}

void Player::split(Shoe *shoe)
{
    unsigned int newtotal = 0;
    unsigned int newsoft = 0;
    newtotal = m_hand.front()->value();
    if (m_hand.front()->isAs())
        newsoft = 1;
    else
        newsoft = newtotal;
    m_total = 0;
    m_softTotal = 0;
    m_total1 = newtotal;
    m_softTotal1 = newsoft;
    m_total2 = newtotal;
    m_softTotal2 = newsoft;
    m_split2.push_back(m_hand.back());
    m_hand.pop_back();
    m_split1.push_back(m_hand.back());
    m_hand.pop_back();
    m_playingHand = 2;
    this->hit(shoe);
    m_playingHand = 1;
    this->hit(shoe);
}

bool Player::hasSplit() const
{
    return (m_playingHand > 0);
}

bool Player::hasSplit1done() const
{
    return (m_playingHand == 2);
}

void Player::split1done()
{
    m_playingHand = 2;
}

short Player::getPlayingHand() const
{
    return m_playingHand;
}

void Player::setPlayingHand(short n)
{
    m_playingHand = n;
}

unsigned int Player::total(short mph) const
{
    if (mph == -1)
        mph = m_playingHand;
    switch (mph) {
    case 0:
        return m_total;
    case 1:
        return m_total1;
    case 2:
        return m_total2;
    default:
        return 0;
    }
}

std::string Player::totalS(short mph) const
{
    unsigned int t = total(mph);
    unsigned int st = softTotal(mph);
    if (t == st)
        return to_string(t);
    else
        return to_string(st) + " / " + to_string(t);
}

unsigned int Player::softTotal(short mph) const
{
    if (mph == -1)
        mph = m_playingHand;
    switch (mph) {
    case 0:
        return m_softTotal;
    case 1:
        return m_softTotal1;
    case 2:
        return m_softTotal2;
    default:
        return 0;
    }
}

bool Player::hasTwentyOne() const
{
    return (this->total() == 21);
}

bool Player::hasBlackJack() const
{
    return (this->total(0) == 21 && m_hand.size() == 2);
}
