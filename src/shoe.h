#ifndef SHOE_H
#define SHOE_H
#include <vector>

class Shoe
{
public:
    Shoe();
    ~Shoe();

    unsigned int getNbCards() const;
    unsigned int at(unsigned int position) const;
    void addCard(unsigned int id); // debug
    void clear();
    void init();
    void shuffle(unsigned int superseed = 0);
    unsigned int draw(bool count = true);
    void setNDeck(unsigned int ndeck);
    unsigned int getNDeck() const;
    void increaseNdeck();
    void decreaseNdeck();
    bool half() const;
    bool quarter() const;
    short rc() const;
    float tc() const;
    void countRC(unsigned int idcard);
    void countFaceDown();

private:
    std::vector<unsigned int> m_shoe;
    unsigned int m_ndeck;
    short m_rc; // runnning count
    short m_id_face_down; // idcard kept for dealer
};

#endif
