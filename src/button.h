#ifndef BUTTON_H
#define BUTTON_H

#include "gameengine.h"
#include <functional>

class Button
{
public:
    Button();
    virtual ~Button();

    bool click(int px, int py) const;
    virtual void show(GameEngine *ge) const = 0;
    virtual bool clickAction(int px, int py);
    void setActive();
    void setInactive();

protected:
    SDL_Rect m_pos;
    bool m_active;
    std::function<void()> onClick;
};

#endif
