#ifndef BUTTON_TEXT_H
#define BUTTON_TEXT_H

#include "button.h"
#include <string>

class ButtonText : public Button
{
public:
    ButtonText();
    ButtonText(int x, int y, int w, int h, const std::function<void()> &click, std::string text, Uint8 r = 255, Uint8 g = 255, Uint8 b = 255);
    ~ButtonText();

    void set(int x, int y, int w, int h, const std::function<void()> &click, std::string text, Uint8 r = 255, Uint8 g = 255, Uint8 b = 255);

    virtual void show(GameEngine *ge) const;

private:
    std::string m_text;
    Uint8 m_r;
    Uint8 m_b;
    Uint8 m_g;
};

#endif
