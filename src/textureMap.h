//
// Created by Keri Southwood-Smith on 10/25/19.
//

#ifndef TEXTUREMAP_H
#define TEXTUREMAP_H

#include "texture.h"
#include <SDL2/SDL_render.h>
#include <map>
#include <string>

class TextureMap
{
public:
    void AddTexture(SDL_Renderer *renderer, const std::string &name, const std::string &filename);
    Texture *GetID(const std::string &name);
    void Cleanup();

private:
    std::map<std::string, Texture *> texmap;
};

#endif // SDLBLACKJACK_TEXTUREMAP_H
