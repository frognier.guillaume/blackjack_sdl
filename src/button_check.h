#ifndef BUTTON_CHECK_H
#define BUTTON_CHECK_H

#include "button.h"

class ButtonCheck : public Button
{
public:
    ButtonCheck();
    ButtonCheck(int x, int y, int w, int h, const std::function<void()> &click, bool state = false);
    ~ButtonCheck();

    void set(int x, int y, int w, int h, const std::function<void()> &click, bool state = false);

    virtual void show(GameEngine *ge) const;
    bool clickAction(int px, int py) override;

private:
    bool m_state;
};

#endif
