#include "button.h"

Button::Button()
{
}

Button::~Button()
{
}

bool Button::click(int px, int py) const
{
    return (m_active && px > m_pos.x && px < m_pos.x + m_pos.w && py > m_pos.y && py < m_pos.y + m_pos.h);
}

bool Button::clickAction(int px, int py)
{
    bool isClick = this->click(px, py);
    if (isClick)
        onClick();
    return isClick;
}

void Button::setActive()
{
    m_active = true;
}

void Button::setInactive()
{
    m_active = false;
}
