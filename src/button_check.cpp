#include "button_check.h"

ButtonCheck::ButtonCheck()
{
}

ButtonCheck::ButtonCheck(int x, int y, int w, int h, const std::function<void()> &click, bool state)
{
    m_pos.x = x;
    m_pos.y = y;
    m_pos.w = w;
    m_pos.h = h;
    onClick = click;
    m_state = state;
    m_active = true;
}

ButtonCheck::~ButtonCheck()
{
}

void ButtonCheck::set(int x, int y, int w, int h, const std::function<void()> &click, bool state)
{
    m_pos.x = x;
    m_pos.y = y;
    m_pos.w = w;
    m_pos.h = h;
    onClick = click;
    m_state = state;
    m_active = true;
}

void ButtonCheck::show(GameEngine *ge) const
{
    if (!m_active)
        return;
    if (m_state)
        ge->fillRect(m_pos, BLACK);
    else {
        ge->fillRect(m_pos, WHITE);
        ge->drawRect(m_pos, BLACK);
    }
}

bool ButtonCheck::clickAction(int px, int py)
{
    bool isClick = this->click(px, py);
    if (isClick) {
        m_state = !m_state;
        onClick();
    }
    return isClick;
}
