#ifndef DEALER_H
#define DEALER_H
#include "card.h"
#include "shoe.h"
#include <vector>

class Dealer
{
public:
    Dealer();
    ~Dealer();

    unsigned int getNbCards() const;
    Card *getCard(unsigned int num) const;
    Card *getLastCard() const;
    Card *getFirstCard() const;
    void hit(Shoe *shoe, bool count = true);
    void seventeen(Shoe *shoe);
    void reset();
    unsigned int total() const;
    unsigned int softTotal() const;
    bool hasTwentyOne() const;
    bool hasBlackJack() const;
    bool hasBust() const;
    bool isShowing() const;
    void flip();
    std::string totalS() const;

private:
    std::vector<Card *> m_hand;
    bool m_showing;
};

#endif
