#ifndef BUTTON_MERE_H
#define BUTTON_MERE_H

#include "button.h"
#include <string>

class ButtonMere : public Button
{
public:
    ButtonMere();
    ButtonMere(int x, int y, int w, int h, const std::function<void()> &click, Uint8 r = 255, Uint8 g = 255, Uint8 b = 255);
    ~ButtonMere();

    void set(int x, int y, int w, int h, const std::function<void()> &click, Uint8 r = 255, Uint8 g = 255, Uint8 b = 255);

    virtual void show(GameEngine *ge) const;

private:
    Uint8 m_r;
    Uint8 m_b;
    Uint8 m_g;
};

#endif
