#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include "textureMap.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <string>
#include <thread>

const static SDL_Color BLACK = {0, 0, 0, 255};
const static SDL_Color DARK_BLUE = {0, 0, 127, 255};
const static SDL_Color DARK_GREEN = {0, 127, 0, 255};
const static SDL_Color DARK_CYAN = {0, 127, 127, 255};
const static SDL_Color DARK_RED = {127, 0, 0, 255};
const static SDL_Color DARK_MAGENTA = {127, 0, 127, 255};
const static SDL_Color DARK_YELLOW = {127, 127, 0, 255};
const static SDL_Color GREY = {127, 127, 127, 255};
const static SDL_Color DARK_GREY = {0, 0, 0, 255};
const static SDL_Color BLUE = {0, 0, 255, 255};
const static SDL_Color GREEN = {0, 255, 0, 255};
const static SDL_Color CYAN = {0, 255, 255, 255};
const static SDL_Color RED = {255, 0, 0, 255};
const static SDL_Color MAGENTA = {255, 0, 255, 255};
const static SDL_Color YELLOW = {255, 255, 0, 255};
const static SDL_Color ORANGE = {255, 165, 0, 255};
const static SDL_Color WHITE = {255, 255, 255, 255};

class GameEngine
{
public:
    // Constructors & Destructors
    GameEngine();
    virtual ~GameEngine();

    void initWindow(int width, int height, bool fullscreen = false);
    void initText();
    void setWindowTitle(unsigned fps);
    void setWindowTitle();

    virtual void handleEvents();
    void setBackGroundColor(Uint8 r, Uint8 g, Uint8 b);
    void render();
    void clearScreen();
    void clean();

    bool running();

    void drawString(std::string sSentence, int x, int y, int size = 15, Uint8 r = 0, Uint8 g = 0, Uint8 b = 0, Uint8 a = SDL_ALPHA_OPAQUE);
    void fillRect(int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void fillRect(SDL_Rect rect, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void fillRect(SDL_Rect rect, SDL_Color col);
    void fillRect(int x, int y, int w, int h, SDL_Color col);
    void drawRect(int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void drawRect(SDL_Rect rect, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void drawRect(SDL_Rect rect, SDL_Color col);
    // void fillTriangle(int xa, int ya, int xb, int yb, int xc, int yc, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    void drawTriangle(int xa, int ya, int xb, int yb, int xc, int yc, Uint8 r, Uint8 g, Uint8 b, Uint8 a = SDL_ALPHA_OPAQUE);
    // void drawGrid(int xs, int ys, int w, int h, int sizex, int sizey, Uint8 r = 0, Uint8 g = 0, Uint8 b = 0, Uint8 a = SDL_ALPHA_OPAQUE);
    // std::string askBox(int x, int y, int w, int h);

    void start();
    void GameThread();
    void quit();

    virtual bool onUserCreate() = 0;
    virtual bool onUserUpdate(float fElapsedTime) = 0;

    void windowResize();
    int windowWidth() const;
    int windowHeight() const;

    void loadTexturesFromDisk(const std::string &name, const std::string &filename);

protected:
    std::string m_sAppName;
    TextureMap m_texmap;

private:
    int m_windowWidth;
    int m_windowHeight;
    bool m_bFont;
    bool m_bRenderNeeded;
    // SDL_Event *event;
    SDL_Window *m_window;
    SDL_Renderer *m_renderer;
    TTF_Font *m_font;
    std::string m_sTitle;
    Uint8 m_bgr;
    Uint8 m_bgg;
    Uint8 m_bgb;

    std::atomic<bool> m_bAtomActive;
    std::condition_variable m_cvGameFinished;
    std::mutex m_muxGame;
};

#endif
