# BlackJack SDL

BlackJack game using SDL libraries.

## Tools needed

- GNU compiler g++
- CMake or GNU Makefile

## Dependencies needed

- SDL2
- SDL2_ttf

## Build

- using CMAKE:
in the root: 
```
mkdir build
cd build
cmake ..
make
```

- using Makefile
in the root:
```
make
```

launch with:
```
./bin/program
```
